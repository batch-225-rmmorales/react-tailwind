import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

export default function App() {
  return (
    <>
      <div className="container">
        <div className="row">
          <h1 className="text-3xl font-bold underline">Hello world!</h1>
        </div>
        <div className="row min-h-[20%] bg-slate-500">
          <p>rafael Norman m. morales</p>
        </div>
        <div className="row min-h-[20%] bg-slate-900">
          <p className="text-white">rafael morales</p>
        </div>
      </div>
    </>
  );
}
